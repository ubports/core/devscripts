#!/bin/bash

HOME_BIN_DIR="${HOME}/bin"

# alternatively you may want to use this instead:
#HOME_BIN_DIR="${HOME}/.local/bin"

if [ -d "${HOME_BIN_DIR}" ]; then
	bindir_exists="true"
else
	mkdir -p "${HOME_BIN_DIR}"
fi

echo
echo "### Preparing your user profile for using the UBports devscripts..."
echo

ln -vfs "$(pwd)/releases/ubports_detectcurrentversion"          "${HOME_BIN_DIR}"
ln -vfs "$(pwd)/devhelpers/ubports_find+replace"                "${HOME_BIN_DIR}"
ln -vfs "$(pwd)/devhelpers/ubports_fix-patch-whitespace"        "${HOME_BIN_DIR}"
ln -vfs "$(pwd)/releases/ubports_mkauthors"                     "${HOME_BIN_DIR}"
ln -vfs "$(pwd)/releases/ubports_mkchangelog"                   "${HOME_BIN_DIR}"
ln -vfs "$(pwd)/releases/ubports_mkdebchangelog"                "${HOME_BIN_DIR}"
ln -vfs "$(pwd)/releases/ubports_mknewsdraft"                   "${HOME_BIN_DIR}"
ln -vfs "$(pwd)/releases/ubports_mkrelease"                     "${HOME_BIN_DIR}"

echo

if [ "x${bindir_exists}" = "x" ]; then
	echo "NOTICE:"
	echo "-------"
	echo "This script just created ${HOME_BIN_DIR}. Logout and login again to include"
	echo "it in your PATH environment variable."
	echo
fi
